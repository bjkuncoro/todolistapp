/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React,{useEffect} from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import store from './src/Redux/Store'
import NameForm from './src/Screens/NameForm';
import ToDo from './src/Screens/ToDo';
import { Provider } from 'react-redux'
import database from '@react-native-firebase/database';
import _ from "lodash";
import { addTaskFromService,setTaskList  } from './src/Redux/Reducer/todoReducer';
import { PersistGate } from 'redux-persist/integration/react'
import { persistStore } from 'redux-persist'

let persistor = persistStore(store);

const App = () => {
  const Stack = createStackNavigator();
  // subscribe()
  const snapshotToArray = (snapshot) => {
    const returnArr = [];
    snapshot.forEach(function(childSnapshot) {
        var item = childSnapshot.val();
        item.key = childSnapshot.key;
        returnArr.push(item);
    });
    return returnArr;
  };
  useEffect(() => {
    // persistor.purge()
    console.log(store.getState())
    database()
    .ref('/todos')
    .on('value', snapshot => {
      console.log(snapshot.val());
      const arr2  = snapshotToArray(snapshot)
      store.dispatch(setTaskList(arr2))
    });
  }, [])

  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <NavigationContainer>
          <Stack.Navigator initialRouteName="NameForm">
            <Stack.Screen name="NameForm" component={NameForm} options={{ headerShown:false }}/>
            <Stack.Screen name="todoScreen" component={ToDo} options={{ headerShown:false }}/>
          </Stack.Navigator>
        </NavigationContainer>
      </PersistGate>
    </Provider>
  );
};



export default App;
