import React,{useState,useEffect} from 'react'
import { View, Text, Modal, TouchableOpacity,TextInput, Image, Platform,ToastAndroid, ActivityIndicator } from 'react-native'
import { Content } from 'native-base'
import { useSelector,useDispatch } from 'react-redux'
import { addTask  } from '../Redux/Reducer/todoReducer';
import { Formik } from 'formik'
import * as yup from 'yup'
import storage from '@react-native-firebase/storage';
import { launchImageLibrary} from 'react-native-image-picker';

const Modalnput = ({visible,parentSetter,taskId}) => {
    const name  =   useSelector(state => state.profile.name)
    const [vis, setvis] = useState(false)
    const [file, setfile] = useState(null)
    const [isLoading, setloading] = useState(false)
    const [imageUrl, setimageUrl] = useState('https://winaero.com/blog/wp-content/uploads/2019/11/Photos-new-icon.png')
    const [title, settitle] = useState('')
    const [desc, setdesc] = useState('')
    const dispatch = useDispatch()

    useEffect(() => {
        setvis(visible)
    }, [visible])

    useEffect(()=>{
        parentSetter(vis)
    },[vis])

    const showToastWithGravity = () => {
        ToastAndroid.showWithGravity(
          "Image Successfully Uploaded" ,
          ToastAndroid.LONG,
          ToastAndroid.BOTTOM,
        );
    };
    
    const ValidationSchema = yup.object().shape({
        title: yup
            .string()
            .min(5, ({ min }) => `Text must be at least ${min} characters`)
            .required('Title is Required'),

        desc: yup
            .string()
            .min(8, ({ min }) => `Text must be at least ${min} characters`)
            .required('Description is Required'),
    })
    
    const filePicker    =  async ()=>{
        launchImageLibrary({mediaType:'photo'}, res => {
            if (res.didCancel) {
                console.log('User cancelled image picker');
            } else if (res.error) {
                console.log('ImagePicker Error: ', res.error);
            } else {
                console.log(res);
                setfile(res)
                // sendImage(res.fileName,res.uri)
            //   setImage(source);
            }
          });
    }

    const sendImage =  (name,uri,par)=>{
        const reference = storage().ref(name);
        const pathToFile    =  (Platform.OS == 'ios') ? decodeURIComponent(uri) : uri
        console.log(pathToFile,name)
        const task = reference.putFile(pathToFile);

        task.on('state_changed', taskSnapshot => {
            console.log(`${taskSnapshot.bytesTransferred} transferred out of ${taskSnapshot.totalBytes}`);
        });

        task.then(async() => {
            console.log('Image uploaded to the bucket!');
            showToastWithGravity()
            const url = await storage() .ref(name) .getDownloadURL();
            setimageUrl(url)
            setTask(par,url)
        });
    }

    const setTask   =   (par,imgurl)=>{
        console.log(name)
        const data  =   {
            id:taskId || Math.floor(Math.random() * 1000),
            title:par.title,
            desc:par.desc, 
            completed:false,
            imageUrl: imgurl,
            createdAt:new Date().toLocaleString(),
            createdBy:name,
        }
        dispatch(addTask(data))
        setvis(false)
        setloading(false)
        settitle('')
        setdesc('')
        setimageUrl('https://winaero.com/blog/wp-content/uploads/2019/11/Photos-new-icon.png')
        setfile(null)
        alert('Task Added.')
    }

    const   triggerFin =   (par)=>{
        setloading(true)
        if(file!==null){
            sendImage(file.fileName,file.uri,par)
        }else{
            setTask(par,imageUrl)
        }
    }

    return (
        <Modal
            onShow={()=>{
                // this.setState({total:this.state.total})
            }}
            animationType='slide'
            transparent
            visible={vis}
        >
            <View style={{width:'100%',height:'100%',backgroundColor:'rgba(0,0,0,0)'}}>
                <View style={{width:'100%',height:'60%',backgroundColor:'#fff',padding:20,borderRadius:15,position:'absolute',bottom:0,elevation:20}}>
                    <View style={{flexDirection:'row'}}>
                        <View style={{flex:4}}>
                            <Text style={{fontFamily:'Quicksand-Bold'}}>
                                Create New Task
                            </Text>
                        </View>
                        <View style={{flex:1,alignItems:'flex-end'}}>
                            <TouchableOpacity onPress={()=>{setvis(false)}} style={{height:30,width:60,borderRadius:10,backgroundColor:'dodgerblue',justifyContent:'center',alignItems:'center'}}>
                                <Text style={{fontFamily:'LexendDeca-Regular',fontSize:10,color:'#0d548c',color:'#fff'}}>
                                    CLOSE
                                </Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <Content style={{marginTop:10}}>
                        <Formik
                            validationSchema={ValidationSchema}
                            initialValues={{ desc:'',title:'' }}
                            onSubmit={values => triggerFin(values)}
                        >
                            {({ handleChange, handleBlur, handleSubmit, values, isValid, errors }) => (
                            <>
                                <View style={{flex:1,flexDirection:'column',alignItems:'center',justifyContent:'space-around',marginBottom:20,marginTop:20,}}>
                                    <TextInput
                                        style={{height: 60,width:'100%', backgroundColor:'#ecf2f3', borderRadius:10 ,paddingLeft:20,paddingRight:20,marginBottom:20}}
                                        placeholder='Task Title'
                                        placeholderTextColor='#8c9596'
                                        underlineColorAndroid='transparent'
                                        autoCapitalize='none'
                                        onBlur={handleBlur('title')}
                                        value={values.title}
                                        // keyboardType='number-pad'
                                        onChangeText={handleChange('title')}            
                                        // onChangeText={(text) => {settitle(text)}}             
                                    />
                                    {errors.title &&
                                        <Text style={{textAlign:'center',fontWeight:'300',fontFamily:'sans-serif-light',fontSize:14,color:'crimson',textAlign:'center'}}>{errors.title}</Text>
                                    }
                                    <TextInput
                                        style={{height: 60,width:'100%', backgroundColor:'#ecf2f3', borderRadius:10 ,paddingLeft:20,paddingRight:20,marginBottom:20}}
                                        placeholder='Description'
                                        placeholderTextColor='#8c9596'
                                        underlineColorAndroid='transparent'
                                        onBlur={handleBlur('desc')}
                                        autoCapitalize='none'
                                        value={values.desc}
                                        // keyboardType='number-pad'
                                        // onChangeText={(text) => {setdesc(text)}} 
                                        onChangeText={handleChange('desc')}            
                                    />
                                    {errors.desc &&
                                        <Text style={{textAlign:'center',fontWeight:'300',fontFamily:'sans-serif-light',fontSize:14,color:'crimson',textAlign:'center'}}>{errors.desc}</Text>
                                    }
                                    <TouchableOpacity onPress={()=>{filePicker()}} style={{width:'100%',height:40,marginVertical:10,borderRadius:10,borderWidth:1,borderColor:'crimson',backgroundColor:'white',justifyContent:'center',alignItems:'center',flexDirection:'row'}}>
                                        <Image
                                            source={file==null?require(`../Assets/Images/add2.png`):require(`../Assets/Images/picture.png`)}
                                            style={{height:20,width:20,marginTop:0}}
                                        />                                                  
                                        <Text style={{fontFamily:'Quicksand-Bold',color:'crimson',}}> { file==null?'Choose File':file.fileName.slice(35) }</Text>
                                    </TouchableOpacity>
                                </View>
                                <TouchableOpacity disabled={!isValid || isLoading} onPress={handleSubmit} style={{flex:1,backgroundColor:'dodgerblue',width:'100%',height:60,elevation:0,borderRadius:10,flexDirection:'column',justifyContent:'space-around',alignItems:'center',paddingVertical:10}}>
                                    <View>
                                        {!isLoading?(
                                        <Text style={{textAlign:'center',fontSize:15,color:'#fff'}}>
                                            Save
                                        </Text>):(
                                            <ActivityIndicator size='large' color="#fff" />
                                        )}
                                    </View>
                                </TouchableOpacity>
                            </>
                            )}
                        </Formik>
                    </Content>
                </View>
            </View>
        </Modal>
    )
}

export default Modalnput
