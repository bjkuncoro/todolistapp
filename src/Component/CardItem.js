import React,{useEffect,useState} from 'react'
import { View, Text, TouchableOpacity, Image } from 'react-native'
import {  removeTask, toggleTask } from '../Redux/Reducer/todoReducer';
import {  useDispatch } from 'react-redux'


const CardItem = ({id,title,desc,createdAt,createdBy,parentSetter,isCompleted,imageUrl}) => {
    const [imgUrl, setimgUrl] = useState('https://cdn1.iconfinder.com/data/icons/picture-sharing-sites-filled/32/No_Image-512.png')
    const dispatch = useDispatch()
    const remove    =   (id)=>{
        dispatch(removeTask(id))
    }
    const todoToggle =   (id)=>{
        dispatch(toggleTask(id))
    }
    // useEffect(()=>{
    //     parentSetter({vis:true,taskId:id})
    // },[taskId])
    return (
        <View style={{width:'100%',height:150,marginVertical:15,flexDirection:'column',alignItems:'center',justifyContent:'center',backgroundColor:'#c6e6ff',borderRadius:20,padding:15}}>
            <View style={{flex:2,width:'100%',flexDirection:'row',justifyContent:'center',alignItems:'center'}}>
                <View style={{flex:3,height:'100%',justifyContent:'flex-start',alignItems:'flex-start'}}>
                    <View style={{width:80,height:80,backgroundColor:'#fff',borderRadius:15,alignItems:'center',justifyContent:'center',overflow:'hidden'}}>
                        <Image
                            // source={{uri:{imageUrl}}}
                            source={{uri:imageUrl}}
                            style={{height:80,width:80,marginTop:0}}
                        />
                    </View>
                </View>
                <View style={{flex:6,height:'100%',flexDirection:'column',justifyContent:'center',alignItems:'flex-start',paddingRight:8}}>
                    <Text style={{textAlign:'center',fontWeight:'300',fontFamily:'sans-serif-medium',fontSize:16,color:'#1d2b6c',textAlign:'left'}}>{title}</Text>
                    <Text style={{textAlign:'center',fontWeight:'300',fontFamily:'sans-serif-light',fontSize:14,color:'#1d2b6c',textAlign:'left'}}>{desc} </Text>
                </View>
                <View style={{flex:2,height:'100%',justifyContent:'center',alignItems:'center'}}>
                    <TouchableOpacity onPress={()=>{todoToggle(id)}} style={{height:'100%',width:'100%',borderRadius:13,alignItems:'center',justifyContent:'center'}}>
                        <Image
                            source={isCompleted?require('../Assets/Images/correct1.png'):require('../Assets/Images/crossed.png')}
                            style={{height:60,width:60,marginTop:0}}
                        />
                    </TouchableOpacity>
                </View>
            </View>
            <View style={{flex:1,width:'100%',flexDirection:'row',justifyContent:'center',alignItems:'center'}}>
                <View style={{flex:2,height:'100%',flexDirection:'row',alignItems:'flex-end',justifyContent:'flex-start',marginTop:10}}>
                    <View style={{flex:1,height:'100%',flexDirection:'column',alignItems:'flex-start',justifyContent:'center'}}>
                        <Image
                            source={require('../Assets/Images/cal.png')}
                            style={{height:20,width:20,marginTop:0}}
                        />
                    </View>
                    <View style={{flex:7,height:'100%',flexDirection:'column',alignItems:'flex-start',justifyContent:'flex-end'}}>
                        <Text style={{textAlign:'center',fontWeight:'300',fontFamily:'sans-serif-light',fontSize:13,color:'#1d2b6c',textAlign:'left'}}>{createdAt}</Text>
                        <Text style={{textAlign:'center',fontWeight:'300',fontFamily:'sans-serif-medium',fontSize:12,color:'#1d2b6c',textAlign:'left'}}>By {createdBy}</Text>
                    </View>
                </View>
                <View style={{flex:1,height:'100%',flexDirection:'row',alignItems:'flex-end',justifyContent:'flex-end'}}>
                    <TouchableOpacity onPress={()=>{parentSetter({vis:true,taskId:id})}} style={{height:'70%',width:30,borderRadius:13,alignItems:'center',justifyContent:'center',marginRight:8}}>
                        <Image
                            source={require('../Assets/Images/edits.png')}
                            style={{height:30,width:30,marginTop:0}}
                        />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=>{remove(id)}} style={{height:'70%',width:30,borderRadius:13,alignItems:'center',justifyContent:'center'}}>
                        <Image
                            source={require('../Assets/Images/remove.png')}
                            style={{height:30,width:30,marginTop:0}}
                        />
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    )
}

export default CardItem
