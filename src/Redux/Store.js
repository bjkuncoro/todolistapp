import {  configureStore, getDefaultMiddleware } from '@reduxjs/toolkit'
import profileReducer from './Reducer/profileReducer';
import {combineReducers} from 'redux'
import todoReducer  from './Reducer/todoReducer';
import {
  persistReducer,
  FLUSH,
  REHYDRATE,
  PAUSE,
  PERSIST,
  PURGE,
  REGISTER
} from 'redux-persist'
import AsyncStorage from '@react-native-community/async-storage';

const reducers  = combineReducers({
  profile : profileReducer,
  todo: todoReducer
})

const persistConfig = {
  key: 'root',
  // version:1,
  storage: AsyncStorage,
  // whitelist:['profile','todo']
};

const persistedReducer = persistReducer(persistConfig, reducers);

const store =  configureStore({
  reducer: persistedReducer,
  middleware: getDefaultMiddleware({
    serializableCheck: {
      ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER]
    }
  })
})

export default store