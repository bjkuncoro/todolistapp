import {  createSlice } from '@reduxjs/toolkit';

const initState   =   {
    name    :   '',
}
// just try using CreateReducer
// export const addName = createAction('ADD_NAME')

// const profileReducer = createReducer(initState, builder => {
//     builder
//       .addCase(addName, (state, action) => {
//         // "mutate" the array by calling push()
//         // state.push(action.payload)
//         state.name  =   action.payload
//       })
//   })
// just try using CreateReducer

export const profileReducer = createSlice({
  name: 'profile',
  initialState: initState,
  reducers: {
    addName: (state,action) => {
        state.name  =   action.payload
        // state.push(action.payload)
    },
  }
})

export const  {addName} = profileReducer.actions
export default profileReducer.reducer;