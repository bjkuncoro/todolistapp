import {  createSlice } from '@reduxjs/toolkit';
import { submitTask,deleteTask } from '../../Service/firebaseService';

const initState   =   [
    // {
    //     id:1,
    //     title:'Kerja Bakti',
    //     desc:'Bekerja bersama di pizza hut', 
    //     completed:false,
    //     createdAt:JSON.stringify(new Date()),
    // },
]

// just try using CreateReducer
// export const addName = createAction('ADD_NAME')

// const profileReducer = createReducer(initState, builder => {
//     builder
//       .addCase(addName, (state, action) => {
//         // "mutate" the array by calling push()
//         // state.push(action.payload)
//         state.name  =   action.payload
//       })
//   })
// just try using CreateReducer

export const todoReducer = createSlice({
  name: 'todo',
  initialState: initState,
  reducers: {
    setTaskList: (state,action) => {
        const newState  =   action.payload
        return newState
    },
    addTaskFromService: (state,action) => {
        const { id, title, desc, completed,imageUrl, createdBy } = action.payload
        const existingPost = state.find(i => i.id === id)
        if(existingPost){
            existingPost.title = title
            existingPost.desc = desc
            existingPost.completed = completed
            existingPost.imageUrl = imageUrl
            existingPost.createdBy = createdBy
        }else{
            state.push(action.payload)
        }
    },
    addTask: (state,action) => {
        // state.push(action.payload)
        const   {id,title,desc,completed,createdAt,imageUrl,createdBy} =   action.payload
        submitTask(id,title,desc,completed,createdAt,imageUrl,createdBy).then(res=>{
            console.log(res)
        }).catch(err=>{
            console.log({err})
        })
    },
    taskUpdated:(state, action)=> {
        const { id, title, desc, imageUrl,createdBy } = action.payload
        const existingPost = state.find(i => i.id === id)
        // if (existingPost) {
        //     existingPost.title = title
        //     existingPost.desc = desc
        // }
        submitTask(id,title,desc,existingPost.completed,existingPost.createdAt,imageUrl,createdBy).then(res=>{
            console.log(res)
        }).catch(err=>{
            console.log({err})
        })
    },
    removeTask: (state,action)=>{
        deleteTask(action.payload).then(res=>{
            console.log(res)
        }).catch(err=>{
            console.log({err})
        })

        const newState = state.filter((i) => i.id !== action.payload)
        return newState
    },
    toggleTask: (state,action)=>{
        const todo = state.find(i => i.id === action.payload)
        submitTask(todo.id,todo.title,todo.desc,!todo.completed,todo.createdAt,todo.imageUrl,todo.createdBy).then(res=>{
            console.log(res)
        }).catch(err=>{
            console.log({err})
        })
    }
  }
})

export const  {addTaskFromService,addTask,taskUpdated,removeTask,toggleTask,setTaskList} = todoReducer.actions
export default todoReducer.reducer;