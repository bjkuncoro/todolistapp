import database from '@react-native-firebase/database';
import _ from "lodash";
import { addTaskFromService  } from '../Redux/Reducer/todoReducer';
// import { useDispatch } from 'react-redux'
import store from '../Redux/Store'

// const dispatch = useDispatch()

export const submitTask = (Id, Title, Desc, Completed, CreatedAt, imageUrl, createdBy) => {
    return new Promise((resolve, reject)=>{
      let key;
      if (Id != null) {
        key = Id;
      } else {
        key = database()
          .ref()
          .push().key;
      }
      let dataToSave = {
        id: key,
        title: Title,
        desc:Desc,
        completed: Completed,
        imageUrl:imageUrl,
        createdAt:CreatedAt,
        createdBy:createdBy,
      };
      database()
        .ref('todos/' + key)
        .update(dataToSave)
        .then(snapshot => {
          resolve(snapshot);
        })
        .catch(err => {
          reject(err);
        });
    });
};

export const deleteTask =   (id)=>{
    return new Promise((resolve,reject)=>{
        database()
        .ref('todos/' + id)
        .remove()
        .then(res=>{
            resolve(res)
        }).catch(err=>{
            reject(err)
        })
    })
}

export const subscribe  =   ()=>{
    database()
    .ref('/todos')
    .on('value', snapshot => {
        console.log(snapshot.val());
        _.map(snapshot.val(), (value, key) => {
            console.log(value)
            store.dispatch(addTaskFromService(value))
        })
    });
}