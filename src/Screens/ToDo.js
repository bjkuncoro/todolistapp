import { Container, Content } from 'native-base'
import React, { useEffect, useState,useCallback } from 'react'
import { View, Text, TouchableOpacity, Image, StatusBar } from 'react-native'
import { useSelector,useStore, useDispatch } from 'react-redux'
import CardItem from '../Component/CardItem'
import Modalnput from '../Component/Modalnput' ;
import ModalUpdate from '../Component/ModalUpdate' ;
import {requestMultiple,checkMultiple, PERMISSIONS} from 'react-native-permissions';

const ToDo = ({navigation}) => {
    const name  =   useSelector(state => state.profile.name)
    const task  =   useSelector(state => state.todo)
    const store =   useStore()
    // const dispatch = useDispatch()
    
    const [updatedId, setupdatedId] = useState()
    const [visible, setvisible] = useState(false)
    const [taskList,settaskList]    =   useState(task)
    const [visUpdate, setvisUpdate] = useState(false)
    const [orderByDate, setOrderByDate] = useState(true)

    useEffect(() => {
        requestMultiple([PERMISSIONS.ANDROID.READ_EXTERNAL_STORAGE,PERMISSIONS.ANDROID.WRITE_EXTERNAL_STORAGE]).then((result) => {
            console.log(result)
          });
        checkMultiple([PERMISSIONS.ANDROID.READ_EXTERNAL_STORAGE,PERMISSIONS.ANDROID.WRITE_EXTERNAL_STORAGE]).then((statuses) => {
            console.log('rr', statuses[PERMISSIONS.ANDROID.READ_EXTERNAL_STORAGE]);
            console.log('ff', statuses[PERMISSIONS.ANDROID.WRITE_EXTERNAL_STORAGE]);
        });
    }, [])

    useEffect(()=>{
        console.log(task)
        settaskList(task)
    },[task])

    const wrapperSetParentStateUpdate = useCallback(
        val => {
            setvisUpdate(val.vis)
            setupdatedId(val.taskId)
        },
        [setupdatedId],
    )

    const wrapperSetParentState = useCallback(
        val => {
            setvisible(val)
        },
        [setvisible],
    )

    const wrapperSetParentStateModalUpdate = useCallback(
        val => {
            setvisUpdate(val)
        },
        [setvisUpdate],
    )
    
    const dynamicSort   =(property)=> {
        var sortOrder = 1;
    
        if(property[0] === "-") {
            sortOrder = -1;
            property = property.substr(1);
        }
    
        return function (a,b) {
            if(sortOrder == -1){
                return b[property].localeCompare(a[property]);
            }else{
                return a[property].localeCompare(b[property]);
            }        
        }
    }

    const sortByAz  =   ()=>{
        let temp =  [].concat(taskList)
        temp.sort(dynamicSort('title'))
        console.log(temp)
        settaskList(temp)
    }
    const sortByDate    =   ()=>{
        let temp =  [].concat(taskList)
        temp.sort((a,b)=>{
            return new Date(b.createdAt) - new Date(a.createdAt);
        });
        console.log(temp)
        settaskList(temp)
    }
    return (
        <Container>
            <ModalUpdate taskId={updatedId} visible={visUpdate} parentSetter={wrapperSetParentStateModalUpdate}/>
            <Modalnput visible={visible} parentSetter={wrapperSetParentState}/>
            <Content style={{backgroundColor:'#fff',paddingHorizontal:20}}>
                <StatusBar backgroundColor='#fff' barStyle="dark-content" />
                <View style={{width:'100%',height:60,padding:10,flexDirection:'row',justifyContent:'space-between',alignItems:'center'}}>
                    <Text style={{textAlign:'center',fontWeight:'bold',fontSize:20,color:'darkslategray'}}>Hi, {name}</Text>
                    <View style={{flex:1,backgroundColor:'#fff',height:'100%',flexDirection:'row',justifyContent:'flex-end',alignItems:'center'}}>
                        <TouchableOpacity onPress={()=>{
                                sortByAz()
                                setOrderByDate(false)
                            }} style={{height:'70%',width:30,borderRadius:13,alignItems:'center',justifyContent:'center',marginRight:20}}>
                            <Image
                                source={require('../Assets/Images/sortaz.png')}
                                style={{height:40,width:40,marginTop:0}}
                            />
                        </TouchableOpacity>
                        <TouchableOpacity onPress={()=>{
                                sortByDate()
                                setOrderByDate(true)
                            }} style={{height:'70%',width:30,borderRadius:13,alignItems:'center',justifyContent:'center'}}>
                            <Image
                                source={require('../Assets/Images/sort.png')}
                                style={{height:40,width:40,marginTop:0}}
                            />
                        </TouchableOpacity>
                    </View>
                </View>
                {taskList.length==0&&<View style={{alignItems:'center',marginTop:20,}}>
                    <Image
                    source={require('../Assets/Images/folder.png')}
                    style={{height:350,width:350,marginTop:0}}
                    />
                    <Text style={{textAlign:'center',fontWeight:'300',fontFamily:'sans-serif-light',fontSize:20,color:'gray'}}>No Task Remaining, Horaay !</Text>
                </View>}
                {taskList.map(i=>{
                    return(
                        <CardItem createdBy={i.createdBy} imageUrl={i.imageUrl} key={i.id} isCompleted={i.completed} id={i.id} title={i.title} desc={i.desc} createdAt={i.createdAt} parentSetter={wrapperSetParentStateUpdate}/>
                    )
                })}
            </Content>
            <View>
                <TouchableOpacity activeOpacity={0.7} onPress={()=>{
                    setvisible(true)
                    }} style={{ position: 'absolute', width: 70, height: 70, alignItems: 'center', justifyContent: 'space-evenly', right: 30, bottom: 40, backgroundColor:'#fff', borderRadius: 15, elevation:7, paddingVertical:5, borderWidth:1.5, borderColor:'lightblue' }}>
                    <Image
                        source={require('../Assets/Images/plus.png')}
                        style={{resizeMode: 'contain', width: 50, height: 50,}}
                    />
                </TouchableOpacity>

            </View> 
        </Container>
    )
}

export default ToDo
