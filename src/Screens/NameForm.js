import { View, Text, TextInput, TouchableOpacity, StatusBar, Image } from 'react-native'
import { Container, Content } from 'native-base'
import React,{useEffect,useState} from 'react'
import { useSelector, useDispatch, useStore } from 'react-redux'
import  {addName} from '../Redux/Reducer/profileReducer'
import { Formik } from 'formik'
import * as yup from 'yup'
// import store from '../Redux/Store'

const NameForm = ({navigation}) => {
    const name      =   useSelector(state => state.profile.name)
    const dispatch  =   useDispatch()
    const store     =   useStore()   
    const [inputname, setinputname] = useState(name)

    const loginValidationSchema = yup.object().shape({
        name: yup
            .string()
            .min(5, ({ min }) => `Password must be at least ${min} characters`)
            .required('Name is Required'),
    })

    const   handleInput =   (name)  =>{
        console.log(name)
        setinputname(name)
        navigation.navigate('todoScreen')
    }

    useEffect(() => {
        dispatch(addName(inputname))
    }, [inputname])

    useEffect(() => {
        if(name){
            navigation.navigate('todoScreen')
        }
    }, [])

    return (
        <Container>
            <StatusBar backgroundColor='#fff' barStyle="dark-content" />
            <Content style={{backgroundColor:'#fff',paddingHorizontal:20}}>
                <View style={{width:'100%',height:60,padding:10,justifyContent:'center',alignItems:'flex-start'}}>
                    <Text style={{textAlign:'center',fontWeight:'bold',fontSize:20,color:'darkslategray'}}>To Do Apps </Text>
                </View>
                <View style={{alignItems:'center',marginTop:20,}}>
                    <Image
                    source={require('../Assets/Images/nameForm-img1.png')}
                    style={{height:350,width:350,marginTop:0}}
                    />
                </View>
                <Formik
                    validationSchema={loginValidationSchema}
                    initialValues={{ name:'' }}
                    onSubmit={values => handleInput(values.name)}
                >
                    {({ handleChange, handleBlur, handleSubmit, values, isValid, errors }) => (
                    <>
                        <View style={{flex:1,flexDirection:'column',alignItems:'center',justifyContent:'space-around',marginBottom:20,marginTop:20,}}>
                            <TextInput
                                name='name'
                                style={{height: 60,width:'100%', backgroundColor:'#ecf2f3', borderRadius:10 ,paddingLeft:20,paddingRight:20,marginBottom:20}}
                                placeholder='Enter Your Name'
                                placeholderTextColor='#8c9596'
                                underlineColorAndroid='transparent'
                                autoCapitalize='none'
                                onBlur={handleBlur('name')}
                                value={values.name}
                                // keyboardType='number-pad'          
                                onChangeText={handleChange('name')}            
                                // onChangeText={(text)=>{handleInput(text)}}             
                            />
                            {errors.name &&
                                <Text style={{textAlign:'center',fontWeight:'300',fontFamily:'sans-serif-light',fontSize:16,color:'crimson',textAlign:'left'}}>{errors.name} </Text>
                            }
                        </View>
                        <TouchableOpacity disabled={!isValid} onPress={handleSubmit} style={{flex:1,backgroundColor:!isValid?'gray':'dodgerblue',width:'100%',height:60,elevation:0,borderRadius:10,flexDirection:'column',justifyContent:'space-around',alignItems:'center',paddingVertical:10}}>
                            <View>
                                <Text style={{textAlign:'center',fontSize:15,color:'#fff'}}>
                                    Let's Go
                                </Text>
                            </View>
                        </TouchableOpacity>
                    </>
                    )}
                </Formik>
            </Content>
        </Container>
    )
}

export default NameForm
